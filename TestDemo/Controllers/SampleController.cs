﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security.Provider;
using TestDemo.Entities;

namespace TestDemo.Controllers
{
    public class SampleController : Controller
    {
        public string GetCompanyNameByCustomerId(string customerId)
        {
            NorthwindEntities db = new NorthwindEntities();
            return db.Customers.SingleOrDefault(c => c.CustomerID == customerId).CompanyName;
        }

        public string UpdateCompanyNameByCustomerId(string customerId, string newCompanyName)
        {
            NorthwindEntities db = new NorthwindEntities();
            var customer = db.Customers.SingleOrDefault(c => c.CustomerID == customerId);
            if (customer != null)
            {
                customer.CompanyName = newCompanyName;
                db.SaveChanges();

                return customer.CompanyName;
            }
            return null;
        }

        public string UpdateCompanyNameByCustomerIdWithIdentityName(string customerId)
        {
            NorthwindEntities db = new NorthwindEntities();
            var customer = db.Customers.SingleOrDefault(c => c.CustomerID == customerId);
            if (customer != null)
            {
                customer.CompanyName = User.Identity.Name;
                db.SaveChanges();

                return customer.CompanyName;
            }
            return null;
        }
    }
}