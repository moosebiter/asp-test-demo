﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDemo.Entities;

namespace TestDemo.Logic
{
    public class MyClass
    {




        IEnumerable<Product> GetProductsInStock(int threshhold)
        {
            var db = new NorthwindEntities();
            return db.Products.Where(p => p.UnitsInStock <= threshhold);
        }
    }
}
