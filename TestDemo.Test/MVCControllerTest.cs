﻿using System;
using System.Linq;

using System.Web.Mvc;
using NUnit.Framework;
using Moq;
using TestDemo.Controllers;
using TestDemo.Entities;

namespace TestDemo.Test
{
    [TestFixture]
    class MVCControllerTest : IntegrationTest   // IntegrationTest is a custom class that allows us to perform methods
                                                  //    within a transaction scope and catch the exceptions from the nested
                                                  //    methods
    {
#region Test Calls
        // The Test methods in this region call private methods that perform the actual tests within a transactionScope
        //  and do not commit the changes. So, by doing so it rolls back the changes in the database.

        [Test]
        public void TestBasicControllerWithEntityFramework()
        {
            PerformTest("TestControllerUpdateWithEF");
        }

        [Test]
        [ExpectedException("System.NullReferenceException")]
        public void TestControllerWithoutUserIdentityMock()
        {
            PerformTest("TestControllerUpdateWithEFandUserIdentity");
        }

        [Test]
        public void TestControllerWithUserIdentityMock()
        {
            PerformTest("TestControllerUpdateWithEFandUserIdentityMock");
        }

#endregion

#region Test Implementations

        // Description: Perform a test using a controller that updates a record using entity framework
        private void TestControllerUpdateWithEF()
        {
            VerifyExecutingInTransaction();

            // get a customer to test with, set an expected value, create the controller
            var db = new NorthwindEntities();
            var customer = db.Customers.First(c => c.CompanyName != null);
            var newCompanyName = "New Company Name";
            var controller = new SampleController();

            // validate that the controller name is not already the expected value
            var originalCompanyName = controller.GetCompanyNameByCustomerId(customer.CustomerID);
            if (originalCompanyName == newCompanyName)
                throw new Exception("Company Name that is to be altered is already altered to expected value.");

            // Perform the method and see if the expected return value is correct
            var updatedCompanyName = controller.UpdateCompanyNameByCustomerId(customer.CustomerID, newCompanyName);
            Assert.AreNotEqual(originalCompanyName, updatedCompanyName, "The expected company name was not returned from the update method");

            // changed because within the scope of the controller instance
            var contextTest = controller.GetCompanyNameByCustomerId(customer.CustomerID);
            Assert.AreNotEqual(originalCompanyName, contextTest, "The company name was not updated within the controller context. This was the main purpose of the method.");

            // The value in the database was not changed because the changes are not committed within the transactionScope
            var dbTest = db.Customers.First(c => c.CustomerID == customer.CustomerID).CompanyName;
            Assert.AreEqual(originalCompanyName, dbTest, "The change was committed to the database within the controller context.");
        }

        // Description: causes error due to no user.identity being defined.
        private void TestControllerUpdateWithEFandUserIdentity()
        {
            VerifyExecutingInTransaction();

            // get a customer to test with and create the controller
            var db = new NorthwindEntities();
            var customer = db.Customers.First(c => c.CompanyName != null);
            var controller = new SampleController();

            // perform the test, which will fail because there is no user identity name in the context
            //  that the controller is executing within
            var originalCompanyName = controller.GetCompanyNameByCustomerId(customer.CustomerID);
            var updatedCompanyName = controller.UpdateCompanyNameByCustomerIdWithIdentityName(customer.CustomerID);
            Assert.AreEqual(originalCompanyName, updatedCompanyName, "The company name was updated when there should have been a null exception");
        }

        // Description: mock the controller context so we can set the user.identity.name and use the controller method
        private void TestControllerUpdateWithEFandUserIdentityMock()
        {
            VerifyExecutingInTransaction();

            // select a customer to test with
            var db = new NorthwindEntities();
            var customer = db.Customers.First(c => c.CompanyName != null);

            // create a mock controller context to set a mocked user identity name up
            var controllerMock = new Mock<ControllerContext>();
            controllerMock.SetupGet(m => m.HttpContext.User.Identity.Name).Returns("IdentityUserName");

            // create the controller and bind the mock context to it
            var controller = new SampleController();
            controller.ControllerContext = controllerMock.Object;

            // perform the test
            var originalCompanyName = controller.GetCompanyNameByCustomerId(customer.CustomerID);
            var updatedCompanyName = controller.UpdateCompanyNameByCustomerIdWithIdentityName(customer.CustomerID);
            Assert.AreNotEqual(originalCompanyName, updatedCompanyName, "The company name was not updated");
        }

#endregion

    }
}
