﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace TestDemo.Test
{
    public class IntegrationTest
    {
        #region Protected Methods

        /// <summary>
        ///     This method uses reflection to call a private method with its parameters
        ///     within a transaction and then disposes it without committing the changes
        /// </summary>
        /// <param name="testFunctionName">The name of the method you want to call</param>
        /// <param name="parameters">an object array of parameters for the method call</param>
        protected virtual void PerformTest(string testFunctionName, object[] parameters = null)
        {
            using (var oTransactionScope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 15, 0)))
            {
                try
                {
                    MethodInfo method = this.GetType()
                        .GetMethod(testFunctionName, BindingFlags.NonPublic | BindingFlags.Instance);

                    if (parameters == null)
                        method.Invoke(this, new object[] {});
                    else
                        method.Invoke(this, parameters);

                    oTransactionScope.Dispose();
                }
                catch (Exception ex)
                {
                    oTransactionScope.Dispose();    // shouldn't be necessary but this is more thorough TODO: implement this line in other projects if it makes sense?

                    if (ex.GetType().Name == "TargetInvocationException")
                        throw ex.InnerException;
                }


            }
        }

        /// <summary>
        ///     Verify that the current operations are occurring within a transaction
        ///     or throw an error. This is intended to be included in each sub method
        ///     to prevent any changes from being committed to the databased
        /// </summary>
        protected void VerifyExecutingInTransaction()
        {
            if (Transaction.Current == null)
                throw new TransactionException("This must be used within a transaction");
        }

        #endregion
    }
}
