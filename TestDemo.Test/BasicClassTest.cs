﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using TestDemo.Entities;

namespace TestDemo.Test
{
    [TestFixture]
    class BasicClassTest : IntegrationTest
    {
        private int _discontinuedProductId { get; set; }
        private int _activeProductId { get; set; }
        private int _testQuantityThreshold { get; set; }
        private Product testProduct { get; set; }

        public BasicClassTest()
        {
            try
            {
                var db = new NorthwindEntities();
                _activeProductId = db.Products.First(p => p.Discontinued == false).ProductID;
                _discontinuedProductId = db.Products.First(p => p.Discontinued == false).ProductID;
                _testQuantityThreshold = 10;
            }
            catch (Exception ex)
            {
                throw new Exception("There are not both active and inactive products in the database which " +
                                    "prevents this test from functioning correctly. Exception: " + ex.Message);
            }
        }

        #region Test Calls

        [Test]
        public void CreateTest()
        {
            PerformTest("CreateProductTest");
        }

        [Test]
        public void RetrieveTest()
        {
            PerformTest("RetrieveProductTest");
        }

        [Test]
        public void UpdateTest()
        {
            PerformTest("UpdateProductTest");
        }

        [Test]
        public void DeleteTest()
        {
            PerformTest("DeleteProductTest");
        }

        [Test]
        public void TestUsingParameters()
        {
            // the ability to pass in parameters was designed into the integration test class to give the developer
            //  the option to use parameters and not limit the way that tests are called. This isn't really used
            //  very much, but it's nice to have the option if a need arises.
            PerformTest("ParameterTest", new object[] { "parameterExample1", _testQuantityThreshold });
            PerformTest("ParameterTest", new object[] { "parameterExample2", _testQuantityThreshold + 10 }); // this is silly and is for demo purposes
        }

        #endregion

        #region Test Implementations

        private void CreateProductTest()
        {
            VerifyExecutingInTransaction();



        }

        private void RetrieveProductTest()
        {
            VerifyExecutingInTransaction();

        }

        private void UpdateProductTest()
        {
            VerifyExecutingInTransaction();

        }

        private void DeleteProductTest()
        {
            VerifyExecutingInTransaction();

        }

        private void ParameterTest(string newValue, int quantityThreshold)
        {
            VerifyExecutingInTransaction();



        }

#endregion

    }
}
