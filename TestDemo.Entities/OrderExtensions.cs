﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDemo.Entities
{
    public static class OrderExtensions
    {
        public static IQueryable<Order> GetOrdersWithProduct(this IQueryable<Order> orders, int? productId = null, string productName = null)
        {
            if (productId == null && productName == null)
                throw new Exception("GetOrdersWithProduct extension method can only be called with a ProductID or a ProductName");

            if(productId != null)
                return orders.Where(o => o.Order_Details.Any(od => od.ProductID == productId));
            return orders.Where(o => o.Order_Details.Any(od => od.Product.ProductName == productName));
        }



    }
}
